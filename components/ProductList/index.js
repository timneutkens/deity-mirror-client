import React from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.scss';

export default ({ products }) => (
  <ul className={styles.list}>
    {products.map(product => (
      <li key={product.id} className="col-sm-6 col-md-3">
        <Link to={product.link}>
          <img src={product.image} className="img-responsive" />
          <h2>{product.name}</h2>
        </Link>
        <strong>{product.price}</strong>
      </li>
    ))}
  </ul>
);
